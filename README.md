Linux is a multi-operating system which can be accessed my many users simultaneously. But this raises security concerns.
For effective security Linux divides authorization into 2 levels: ownership and permission.
Ownership of linux files: every file or directory in Linux is assigned 3 types of owner : user/owner, group, other.
Permission of linux files : r(read), w(write),x(execute).
To change file permissions we can use chmod command like this : chmod o=rwx filename for user/owner 
